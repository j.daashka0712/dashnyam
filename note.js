$(document).ready(function() {
    var noteArray = [],
        count = 0;
    function listRefresh() {
        $('#list').empty();
        for(var i = 0; i < noteArray.length; i++) {
            var name = noteArray[i].name,
                date = noteArray[i].date,
                dateString,
                month,
                element;
            month = date.getMonth() + 1;
            dateString = date.getDate() + "/" + month + "/" + date.getFullYear();
            element = $('<li data-id="' + noteArray[i].id + '" data-name="' + noteArray[i].name + '">');
            element.append($('<div class="div-name">').text(name));
            element.append($('<div class="div-date">').text(dateString));
            $('#list').append(element);
        }
    }
    $('#list').on('click', 'li', function() {
        var id = $(this).data('id'),
            content = '',
            name = $(this).data('name');
        console.log("name: ", name)
        $('#list li.selected').removeClass('selected');
        $(this).addClass('selected');
        for (var i = 0; i < noteArray.length; i++) {
            if (noteArray[i].id === id) {
                content = noteArray[i].content;
            }
        }
        $('#edit-name').val(name);
        $('#edit-content').val(content);
        $('#div-edit').removeClass('hide');
    })
    $('#add').on('click', function() {
        var name = $('#name').val(),
            content = $('#content').val(),
            date = new Date();
        if (name === "") {
            alert("Temdegleliin neree ugnu uu?");
        } else {
            noteArray.push({
                id: count,
                name: name,
                content: content,
                date: date
            })
        }
        count++;
        $('#content').val('');
        $('#name').val('');
        listRefresh();
    })
    $('#save').on('click', function() {
        var id = $('#list li.selected').data('id'),
            name = $('#edit-name').val(),
            content = $('#edit-content').val();
        for (var i = 0; i < noteArray.length; i++) {
            if (noteArray[i].id === id) {
                noteArray[i].name = name;
                noteArray[i].content = content;
                break;
            }
        }
        listRefresh();
        $('#list li[data-id="' + id + '"]').addClass('selected');
    })
    $('#cancel').on('click', function() {
        $('#div-edit').addClass('hide');
        $('#list li.selected').removeClass('selected');
    })
    $('#remove').on('click', function() {
        var id = $('#list li.selected').data('id');
        var r = confirm('Ta temdeglelee ustgahdaa itgeltei baina uu?');
        if (r) {
            for (var i = 0; i < noteArray.length; i++) {
                if (noteArray[i].id === id) {
                    noteArray.splice(i, 1);
                    break;
                }
            }
            listRefresh();
            $('#div-edit').addClass('hide');
        }
    })
});